
<?php
	$db = new PDO('mysql:host=localhost; dbname=base', 'root', ''); // Connection to DataBase
?>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>РОУТЕР "Біля Башні"</title>
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<style type="text/css">
body{
	width: 95%;
	
	display: flex;
	justify-content: center;

}
#wrapper{
	box-sizing: border-box;
	width: 95%;
	margin: 5 auto;
	padding:0;
	display:flex;
	flex-flow: column wrap;
}
header{
	display: flex;
	flex-direction: column;
	align-items: center;
	
}

header span{
	padding: 5px;
	font-size: 24pt;
	color: white;
	font-weight: 800;
	text-decoration: none;
	background-color: black;
	box-shadow: inset 2px 2px 5px red;
	margin: 5px;
}

header a{
	padding: 5px;
	font-size: 16pt;
	color: white;
	font-weight: 800;
	text-decoration: none;
	background-color: red;
	box-shadow: inset 2px 2px 5px black;
	margin: 5px;
}

main{
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap;
}
main div{
	display: flex;
	border: 2px solid red;
	align-items: center;
	flex-direction: column;
}
main div span{
	padding: 5px;
	font-size: 24pt;
	color: white;
	font-weight: 800;
	text-decoration: none;
	background-color: red;
	box-shadow: inset 2px 2px 5px black;
}
img{
	width: 100%;
}
td img {
	width: 50px;
	
}
table{
    border-collapse: collapse;
}
td{
	border: 1px solid black;
	text-align: center;
}



header a:hover{
	padding: 7px;
	font-size: 24pt;
	color: white;
	font-weight: 800;
	background-color: red;
	box-shadow: inset 4px 4px 4px 5px black;
	margin: 5px;
}
	</style>
</head>
<body>
<div id="wrapper">

	<header>
		<span>РОУТЕР "Біля Башні"</span>
	</header>
	<img src="images/splashAnime.gif" alt="" style="width:600px;">
	<main>
		<?php
			$sql = "SELECT * FROM `justIn`";
			$res = $db -> query($sql);
			$justIn = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Прості підключення до Wi-Fi:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
							<th>Name</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($justIn as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";
					echo "<td>" . $value['name'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";
	?>

		<?php
			$sql = "SELECT * FROM `phones`";
			$res = $db -> query($sql);
			$phones = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Телефонували за роботу:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($phones as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";


			$sql = "SELECT * FROM `android`";
			$res = $db -> query($sql);
			$android = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Завантажили додаток Android:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($android as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";

			$sql = "SELECT * FROM `iphone`";
			$res = $db -> query($sql);
			$iphone = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Завантажили додаток iOS:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($iphone as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";
	?>
	</main>
				
	

	<header>
		<span>РОУТЕР "MICROTIK у Тані"</span>
	</header>
	<?php
	$db = new PDO('mysql:host=localhost; dbname=microtik1', 'root', ''); // Connection to DataBase
?>
	<main>
		<?php
			$sql = "SELECT * FROM `justIn`";
			$res = $db -> query($sql);
			$justIn = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Прості підключення до Wi-Fi:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
							<th>Name</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($justIn as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";
					echo "<td>" . $value['name'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";
	?>

		<?php
			$sql = "SELECT * FROM `phones`";
			$res = $db -> query($sql);
			$phones = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Телефонували за роботу:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($phones as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";


			$sql = "SELECT * FROM `android`";
			$res = $db -> query($sql);
			$android = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Завантажили додаток Android:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($android as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";

			$sql = "SELECT * FROM `iphone`";
			$res = $db -> query($sql);
			$iphone = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Завантажили додаток iOS:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($iphone as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";
	?>
	</main>


			
	<header>
		<span>РОУТЕР "740"</span>
	</header>
	<?php
	$db = new PDO('mysql:host=localhost; dbname=740', 'root', ''); // Connection to DataBase
?>
	<main>
		<?php
			$sql = "SELECT * FROM `justIn`";
			$res = $db -> query($sql);
			$justIn = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Прості підключення до Wi-Fi:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
							<th>Name</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($justIn as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";
					echo "<td>" . $value['name'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";
	?>

		<?php
			$sql = "SELECT * FROM `phones`";
			$res = $db -> query($sql);
			$phones = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Телефонували за роботу:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($phones as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";


			$sql = "SELECT * FROM `android`";
			$res = $db -> query($sql);
			$android = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Завантажили додаток Android:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($android as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";

			$sql = "SELECT * FROM `iphone`";
			$res = $db -> query($sql);
			$iphone = $res -> fetchAll(PDO::FETCH_ASSOC);
			
			echo "<div><h1>Завантажили додаток iOS:</h1><table>";
				echo "<thead>
						<tr>
							<th>№ п/п</th>
							<th>Дата</th>
							<th>mac Адреса</th>
						</tr>
					<tbody>";
				$n=0;
			foreach ($iphone as $value) {
				$n++;
				echo "<tr>";
					echo "<td>" .$n. "</td>";
					echo "<td>" . $value['date'] . "</td>";
					echo "<td>" . $value['mac'] . "</td>";

				echo "</tr>";
			}
			echo "</tbody></table></div>";
	?>
	</main>
</div>   

</body>
</html>
