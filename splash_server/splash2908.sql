-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Сер 29 2018 р., 12:16
-- Версія сервера: 10.1.34-MariaDB
-- Версія PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `740`
--
CREATE DATABASE IF NOT EXISTS `740` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `740`;

-- --------------------------------------------------------

--
-- Структура таблиці `android`
--

CREATE TABLE `android` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `android`
--

INSERT INTO `android` (`id`, `date`, `mac`) VALUES
(31, '2018-08-05 07:56:01', 'ac:63:be:b4:71:6d'),
(32, '2018-08-07 13:09:52', '58:a2:b5:f3:32:9e'),
(33, '2018-08-16 08:49:50', '08:78:08:37:7d:d8'),
(34, '2018-08-16 10:53:42', 'e8:93:09:a2:ec:0a'),
(35, '2018-08-17 07:17:10', '08:78:08:37:7d:d8'),
(36, '2018-08-22 09:34:20', '3c:dc:bc:03:8e:42'),
(37, '2018-08-22 19:07:35', 'a0:32:99:96:5b:93'),
(38, '2018-08-24 16:21:43', '08:78:08:37:7d:d8'),
(39, '2018-08-26 15:14:12', '08:78:08:37:7d:d8'),
(40, '2018-08-27 04:43:18', '08:78:08:37:7d:d8'),
(41, '2018-08-27 17:55:57', '9c:2e:a1:e2:78:4d'),
(42, '2018-08-28 17:18:04', 'd0:65:ca:e5:29:75');

-- --------------------------------------------------------

--
-- Структура таблиці `in`
--

CREATE TABLE `in` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `iphone`
--

CREATE TABLE `iphone` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `iphone`
--

INSERT INTO `iphone` (`id`, `date`, `mac`) VALUES
(1, '2018-08-22 19:07:31', 'a0:32:99:96:5b:93');

-- --------------------------------------------------------

--
-- Структура таблиці `justin`
--

CREATE TABLE `justin` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `justin`
--

INSERT INTO `justin` (`id`, `date`, `mac`, `name`) VALUES
(22, '2018-08-03 15:17:11', '74:04:2b:61:01:b6', ''),
(23, '2018-08-03 17:09:55', '48:88:ca:88:7b:af', ''),
(24, '2018-08-04 00:55:22', 'ac:63:be:b4:71:6d', ''),
(25, '2018-08-04 05:12:05', '38:a4:ed:aa:1e:a5', ''),
(26, '2018-08-04 09:46:26', '38:a4:ed:aa:1e:a5', ''),
(27, '2018-08-04 15:14:54', '38:a4:ed:aa:1e:a5', ''),
(28, '2018-08-04 17:39:25', 'ac:c1:ee:80:67:b1', ''),
(29, '2018-08-05 05:44:26', '38:a4:ed:aa:1e:a5', ''),
(30, '2018-08-05 11:18:27', '8c:29:37:42:0e:ce', ''),
(31, '2018-08-05 11:29:02', '38:a4:ed:aa:1e:a5', ''),
(32, '2018-08-05 16:52:03', '48:88:ca:88:7b:af', ''),
(33, '2018-08-06 02:10:51', '48:88:ca:88:7b:af', ''),
(34, '2018-08-06 15:37:07', '64:db:43:4e:0c:66', ''),
(35, '2018-08-06 16:46:03', '48:88:ca:88:7b:af', ''),
(36, '2018-08-07 06:41:57', '74:04:2b:61:01:b6', ''),
(37, '2018-08-07 07:30:43', 'd8:32:e3:ad:cb:66', ''),
(38, '2018-08-07 09:29:13', '50:bc:96:c4:f0:b7', ''),
(39, '2018-08-07 17:22:40', '38:a4:ed:aa:1e:a5', ''),
(40, '2018-08-07 18:30:57', 'ac:63:be:b4:71:6d', ''),
(41, '2018-08-08 03:43:16', '38:a4:ed:aa:1e:a5', ''),
(42, '2018-08-08 10:02:46', '74:04:2b:61:01:b6', ''),
(43, '2018-08-08 14:08:05', '84:be:52:de:0b:05', ''),
(44, '2018-08-08 16:56:00', '48:88:ca:88:7b:af', ''),
(45, '2018-08-09 04:44:34', '68:3e:34:40:b7:0f', ''),
(46, '2018-08-09 04:53:47', '38:a4:ed:aa:1e:a5', ''),
(47, '2018-08-09 09:02:43', 'c4:0b:cb:e4:cc:34', ''),
(48, '2018-08-09 09:33:13', '00:4d:44:15:2e:04', ''),
(49, '2018-08-09 17:39:25', 'ac:63:be:b4:71:6d', ''),
(50, '2018-08-10 15:23:27', 'bc:76:5e:46:ea:3b', ''),
(51, '2018-08-10 15:44:11', 'b0:e2:35:77:d4:f9', ''),
(52, '2018-08-10 16:39:32', '48:88:ca:88:7b:af', ''),
(53, '2018-08-10 16:48:26', '38:a4:ed:aa:1e:a5', ''),
(54, '2018-08-11 06:34:48', 'ac:63:be:b4:71:6d', ''),
(55, '2018-08-11 06:59:14', '8c:29:37:42:0e:ce', ''),
(56, '2018-08-11 12:17:01', '8c:29:37:42:0e:ce', ''),
(57, '2018-08-11 14:28:34', '8c:29:37:42:0e:ce', ''),
(58, '2018-08-11 17:00:51', 'bc:76:5e:46:ea:3b', ''),
(59, '2018-08-11 17:11:23', '48:88:ca:88:7b:af', ''),
(60, '2018-08-11 19:18:21', '8c:29:37:42:0e:ce', ''),
(61, '2018-08-12 04:53:22', 'a4:93:3f:44:42:0b', ''),
(62, '2018-08-12 16:52:49', 'bc:76:5e:46:ea:3b', ''),
(63, '2018-08-12 16:57:14', '38:a4:ed:aa:1e:a5', ''),
(64, '2018-08-12 18:25:29', 'ac:63:be:b4:71:6d', ''),
(65, '2018-08-13 05:58:35', 'b0:e2:35:77:d4:f9', ''),
(66, '2018-08-13 07:09:06', '18:f0:e4:1d:1c:e0', ''),
(67, '2018-08-13 07:39:28', '54:27:58:9c:d2:44', ''),
(68, '2018-08-13 16:45:32', '38:a4:ed:aa:1e:a5', ''),
(69, '2018-08-13 17:10:59', '48:88:ca:88:7b:af', ''),
(70, '2018-08-13 17:24:42', '6c:5f:1c:27:a6:64', ''),
(71, '2018-08-14 01:51:34', '38:a4:ed:aa:1e:a5', ''),
(72, '2018-08-14 06:24:41', '24:92:0e:96:09:ac', ''),
(73, '2018-08-14 06:26:24', '80:ad:16:68:2c:00', ''),
(74, '2018-08-14 06:45:41', '34:8a:7b:9c:04:a2', ''),
(75, '2018-08-14 06:51:22', 'b0:e2:35:77:d4:f9', ''),
(76, '2018-08-14 07:32:36', '64:db:43:4e:0c:66', ''),
(77, '2018-08-14 11:12:47', '8c:29:37:42:0e:ce', ''),
(78, '2018-08-14 12:31:29', '24:92:0e:96:09:ac', ''),
(79, '2018-08-14 13:58:29', '8c:29:37:42:0e:ce', ''),
(80, '2018-08-14 16:24:03', 'ac:63:be:b4:71:6d', ''),
(81, '2018-08-14 16:38:43', 'bc:76:5e:46:ea:3b', ''),
(82, '2018-08-14 17:42:32', '8c:29:37:42:0e:ce', ''),
(83, '2018-08-15 05:25:10', '8c:29:37:42:0e:ce', ''),
(84, '2018-08-15 06:15:57', 'b0:e2:35:77:d4:f9', ''),
(85, '2018-08-15 09:59:56', '90:21:81:eb:34:c8', ''),
(86, '2018-08-15 13:23:06', 'b0:e2:35:77:d4:f9', ''),
(87, '2018-08-15 17:02:30', '48:88:ca:88:7b:af', ''),
(88, '2018-08-16 07:14:20', 'b0:e2:35:77:d4:f9', ''),
(89, '2018-08-16 09:28:26', '3c:05:18:8c:ef:6f', ''),
(90, '2018-08-16 10:06:43', '64:db:43:4e:0c:66', ''),
(91, '2018-08-16 11:31:59', 'd8:32:e3:ae:3b:6e', ''),
(92, '2018-08-16 12:07:02', '50:55:27:da:12:4b', ''),
(93, '2018-08-16 12:10:35', 'c4:62:ea:64:36:ea', ''),
(94, '2018-08-16 12:15:36', '14:d1:1f:7d:a1:be', ''),
(95, '2018-08-16 12:48:42', '24:92:0e:96:09:ac', ''),
(96, '2018-08-16 14:47:06', 'b8:57:d8:64:5b:5e', ''),
(97, '2018-08-16 17:04:08', '6c:5f:1c:27:a6:64', ''),
(98, '2018-08-16 17:04:09', '6c:5f:1c:27:a6:64', ''),
(99, '2018-08-16 17:22:43', 'ac:63:be:b4:71:6d', ''),
(100, '2018-08-17 07:02:20', '08:78:08:6d:a5:64', ''),
(101, '2018-08-17 10:34:33', '64:db:43:4e:0c:66', ''),
(102, '2018-08-17 11:15:36', '8c:29:37:42:0e:ce', ''),
(103, '2018-08-17 12:45:22', 'b0:55:08:6c:aa:48', ''),
(104, '2018-08-17 12:45:34', 'b0:55:08:6c:aa:48', ''),
(105, '2018-08-17 16:53:25', '48:88:ca:88:7b:af', ''),
(106, '2018-08-17 16:53:35', 'bc:76:5e:46:ea:3b', ''),
(107, '2018-08-17 16:57:29', '6c:5f:1c:27:a6:64', ''),
(108, '2018-08-17 16:57:32', '6c:5f:1c:27:a6:64', ''),
(109, '2018-08-17 16:57:32', '6c:5f:1c:27:a6:64', ''),
(110, '2018-08-18 05:12:52', '38:a4:ed:aa:1e:a5', ''),
(111, '2018-08-18 08:50:07', '38:a4:ed:aa:1e:a5', ''),
(112, '2018-08-18 17:04:54', 'ac:63:be:b4:71:6d', ''),
(113, '2018-08-18 18:31:59', '20:47:da:1e:04:4b', ''),
(114, '2018-08-20 08:20:45', 'b0:e2:35:77:d4:f9', ''),
(115, '2018-08-20 08:48:31', '8c:29:37:42:0e:ce', ''),
(116, '2018-08-20 09:34:48', 'b0:e2:35:77:d4:f9', ''),
(117, '2018-08-20 11:02:19', '8c:29:37:42:0e:ce', ''),
(118, '2018-08-20 13:08:58', '64:db:43:4e:0c:66', ''),
(119, '2018-08-20 16:11:49', 'bc:76:5e:46:ea:3b', ''),
(120, '2018-08-20 16:55:39', 'ac:63:be:b4:71:6d', ''),
(121, '2018-08-21 08:08:58', '00:00:00:53:55:8d', ''),
(122, '2018-08-21 09:22:19', '64:db:43:4e:0c:66', ''),
(123, '2018-08-21 09:47:19', '80:ad:16:7d:d2:84', ''),
(124, '2018-08-22 16:40:31', 'bc:76:5e:46:ea:3b', ''),
(125, '2018-08-22 17:17:46', 'ac:63:be:b4:71:6d', ''),
(126, '2018-08-23 04:59:23', '38:a4:ed:aa:1e:a5', ''),
(127, '2018-08-23 06:43:32', '8c:29:37:42:0e:ce', ''),
(128, '2018-08-23 08:20:32', '38:a4:ed:aa:1e:a5', ''),
(129, '2018-08-23 08:49:49', '8c:29:37:42:0e:ce', ''),
(130, '2018-08-23 13:33:10', '1c:b7:2c:9c:f6:d4', ''),
(131, '2018-08-23 15:00:38', '48:88:ca:88:7b:af', ''),
(132, '2018-08-23 17:34:31', 'bc:76:5e:46:ea:3b', ''),
(133, '2018-08-24 07:27:34', '8c:29:37:42:0e:ce', ''),
(134, '2018-08-24 16:29:12', 'bc:76:5e:46:ea:3b', ''),
(135, '2018-08-24 17:03:07', '48:88:ca:0e:7c:11', ''),
(136, '2018-08-24 17:35:37', 'ac:63:be:b4:71:6d', ''),
(137, '2018-08-25 14:23:25', '8c:29:37:42:0e:ce', ''),
(138, '2018-08-25 16:46:01', '48:88:ca:88:7b:af', ''),
(139, '2018-08-26 05:19:26', 'ac:63:be:b4:71:6d', ''),
(140, '2018-08-26 05:21:05', 'bc:76:5e:46:ea:3b', ''),
(141, '2018-08-26 06:59:07', '8c:29:37:42:0e:ce', ''),
(142, '2018-08-26 09:57:02', 'c0:64:c6:4e:68:ac', ''),
(143, '2018-08-26 13:35:20', '00:fc:f6:0f:24:8c', ''),
(144, '2018-08-26 16:25:47', '48:88:ca:0e:7c:11', ''),
(145, '2018-08-26 16:54:35', '48:88:ca:88:7b:af', ''),
(146, '2018-08-27 06:29:40', '00:eb:2d:71:fa:23', ''),
(147, '2018-08-27 08:54:12', 'b4:52:7e:ee:a4:80', ''),
(148, '2018-08-27 12:06:47', 'd8:6c:02:9c:6b:2c', ''),
(149, '2018-08-27 17:33:18', 'ac:63:be:b4:71:6d', ''),
(150, '2018-08-28 14:38:13', '00:f8:84:92:c4:da', ''),
(151, '2018-08-28 16:59:48', '48:88:ca:0e:7c:11', ''),
(152, '2018-08-28 17:05:54', '48:88:ca:88:7b:af', ''),
(153, '2018-08-28 17:35:02', '54:fc:f0:8a:85:07', ''),
(154, '2018-08-29 06:27:38', '18:3a:2d:44:c9:92', '');

-- --------------------------------------------------------

--
-- Структура таблиці `phones`
--

CREATE TABLE `phones` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `android`
--
ALTER TABLE `android`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `in`
--
ALTER TABLE `in`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `iphone`
--
ALTER TABLE `iphone`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `justin`
--
ALTER TABLE `justin`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `android`
--
ALTER TABLE `android`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT для таблиці `in`
--
ALTER TABLE `in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `iphone`
--
ALTER TABLE `iphone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `justin`
--
ALTER TABLE `justin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT для таблиці `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- База даних: `base`
--
CREATE DATABASE IF NOT EXISTS `base` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `base`;

-- --------------------------------------------------------

--
-- Структура таблиці `android`
--

CREATE TABLE `android` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `android`
--

INSERT INTO `android` (`id`, `date`, `mac`) VALUES
(31, '2018-08-14 05:45:45', 'd8:6c:02:ac:75:8c');

-- --------------------------------------------------------

--
-- Структура таблиці `in`
--

CREATE TABLE `in` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `iphone`
--

CREATE TABLE `iphone` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `justin`
--

CREATE TABLE `justin` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `justin`
--

INSERT INTO `justin` (`id`, `date`, `mac`, `name`) VALUES
(2, '2018-07-31 13:07:50', '98:0c:a5:20:13:b4', 'work'),
(6, '2018-07-31 14:05:55', '34:80:b3:3e:63:b7', 'Sasha'),
(7, '2018-07-31 16:41:11', 'f8:84:f2:55:1e:57', ''),
(8, '2018-08-01 06:25:46', '34:80:b3:3e:63:b7', ''),
(9, '2018-08-01 10:25:38', '34:80:b3:3e:63:b7', ''),
(10, '2018-08-01 11:35:35', '34:80:b3:3e:63:b7', ''),
(11, '2018-08-02 06:14:05', '34:80:b3:3e:63:b7', ''),
(13, '2018-08-02 17:20:08', '50:8f:4c:44:b2:dd', ''),
(14, '2018-08-02 22:45:45', '50:8f:4c:44:b2:dd', ''),
(15, '2018-08-03 04:50:37', '00:08:22:56:08:fc', ''),
(16, '2018-08-03 06:03:45', '00:08:22:56:08:fc', ''),
(17, '2018-08-03 06:28:45', '34:80:b3:3e:63:b7', ''),
(18, '2018-08-03 09:37:22', '00:08:22:68:fb:fb', ''),
(19, '2018-08-03 12:32:59', '00:08:22:68:fb:fb', ''),
(20, '2018-08-04 05:02:00', '50:8f:4c:44:b2:dd', ''),
(21, '2018-08-04 11:20:37', '50:8f:4c:44:b2:dd', ''),
(22, '2018-08-05 00:11:11', '98:0c:a5:20:13:b4', ''),
(23, '2018-08-05 04:43:41', '00:08:22:68:fb:fb', ''),
(24, '2018-08-05 06:31:25', '00:08:22:68:fb:fb', ''),
(25, '2018-08-05 14:38:49', '00:08:22:68:fb:fb', ''),
(26, '2018-08-06 07:22:24', '34:80:b3:3e:63:b7', ''),
(27, '2018-08-06 11:58:41', '34:80:b3:3e:63:b7', ''),
(28, '2018-08-06 13:47:00', '34:80:b3:3e:63:b7', ''),
(29, '2018-08-07 02:58:09', '00:08:22:68:fb:fb', ''),
(30, '2018-08-07 05:09:09', '50:8f:4c:44:b2:dd', ''),
(31, '2018-08-07 05:15:34', '98:0c:a5:20:13:b4', ''),
(32, '2018-08-07 05:35:26', '34:80:b3:3e:63:b7', ''),
(33, '2018-08-07 08:17:07', '34:80:b3:3e:63:b7', ''),
(34, '2018-08-07 11:37:28', '50:8f:4c:44:b2:dd', ''),
(35, '2018-08-07 12:33:49', '98:0c:a5:20:13:b4', ''),
(36, '2018-08-07 14:40:24', '34:80:b3:3e:63:b7', ''),
(37, '2018-08-08 04:35:25', '00:08:22:68:fb:fb', ''),
(38, '2018-08-08 06:23:07', '34:80:b3:3e:63:b7', ''),
(39, '2018-08-08 08:26:54', '00:08:22:68:fb:fb', ''),
(40, '2018-08-08 09:13:02', '34:80:b3:3e:63:b7', ''),
(41, '2018-08-08 13:01:45', '34:80:b3:3e:63:b7', ''),
(42, '2018-08-09 05:11:39', 'd8:63:75:8c:61:1b', ''),
(43, '2018-08-09 05:58:19', 'd8:63:75:8c:61:1b', ''),
(44, '2018-08-09 06:10:31', '34:80:b3:3e:63:b7', ''),
(45, '2018-08-09 11:14:15', '34:80:b3:3e:63:b7', ''),
(46, '2018-08-09 12:56:27', '34:80:b3:3e:63:b7', ''),
(47, '2018-08-09 17:06:17', '00:08:22:9e:24:fc', ''),
(48, '2018-08-09 23:26:46', '00:08:22:9e:24:fc', ''),
(49, '2018-08-10 06:53:03', '34:80:b3:3e:63:b7', ''),
(50, '2018-08-10 08:09:46', 'bc:54:36:0d:ad:ae', ''),
(51, '2018-08-10 09:03:14', 'bc:54:36:0d:ad:ae', ''),
(52, '2018-08-10 09:03:18', 'bc:54:36:0d:ad:ae', ''),
(53, '2018-08-10 13:02:18', '34:80:b3:3e:63:b7', ''),
(54, '2018-08-10 14:06:58', '50:8f:4c:44:b2:dd', ''),
(55, '2018-08-10 20:07:35', '50:8f:4c:44:b2:dd', ''),
(56, '2018-08-11 02:51:59', '50:8f:4c:44:b2:dd', ''),
(57, '2018-08-11 04:46:43', '00:08:22:14:09:fc', ''),
(58, '2018-08-11 07:47:56', '00:08:22:14:09:fc', ''),
(59, '2018-08-11 17:13:45', '34:80:b3:3e:63:b7', ''),
(60, '2018-08-11 20:54:47', '98:0c:a5:20:13:b4', ''),
(61, '2018-08-11 23:32:02', '34:80:b3:3e:63:b7', ''),
(62, '2018-08-12 04:37:57', '00:08:22:14:09:fc', ''),
(63, '2018-08-12 07:41:01', '00:08:22:14:09:fc', ''),
(64, '2018-08-12 11:08:40', '00:08:22:14:09:fc', ''),
(65, '2018-08-13 04:33:36', '00:08:22:14:09:fc', ''),
(66, '2018-08-13 05:02:17', '50:8f:4c:44:b2:dd', ''),
(67, '2018-08-13 07:16:56', '00:08:22:14:09:fc', ''),
(68, '2018-08-13 08:51:10', '00:08:22:14:09:fc', ''),
(69, '2018-08-13 09:43:05', '00:08:22:14:09:fc', ''),
(70, '2018-08-13 11:14:40', '50:8f:4c:44:b2:dd', ''),
(71, '2018-08-14 06:06:29', 'c4:0b:cb:48:4e:7d', ''),
(72, '2018-08-14 08:23:43', 'bc:76:5e:06:7d:57', ''),
(73, '2018-08-14 09:04:48', 'f4:60:e2:9c:7a:cc', ''),
(74, '2018-08-14 09:05:06', '00:ee:bd:66:fa:7c', ''),
(75, '2018-08-14 09:21:45', '00:ec:0a:bf:16:d3', ''),
(76, '2018-08-14 10:10:03', '34:80:b3:3e:63:b7', ''),
(77, '2018-08-14 10:13:36', 'f4:60:e2:9c:7a:cc', ''),
(78, '2018-08-14 12:39:03', 'f4:60:e2:9c:7a:cc', ''),
(79, '2018-08-14 12:53:28', '98:0c:a5:20:13:b4', ''),
(80, '2018-08-14 18:12:01', '34:80:b3:3e:63:b7', ''),
(81, '2018-08-15 05:01:13', '50:8f:4c:44:b2:dd', ''),
(82, '2018-08-15 07:26:22', 'f4:60:e2:9c:7a:cc', ''),
(83, '2018-08-15 07:30:31', '00:ee:bd:66:fa:7c', ''),
(84, '2018-08-15 07:46:44', '2c:fd:ab:13:aa:19', ''),
(85, '2018-08-15 08:21:59', 'f4:60:e2:9c:7a:cc', ''),
(86, '2018-08-15 10:21:42', 'f4:60:e2:9c:7a:cc', ''),
(87, '2018-08-15 11:00:15', '50:8f:4c:44:b2:dd', ''),
(88, '2018-08-15 14:31:28', '50:8f:4c:44:b2:dd', ''),
(89, '2018-08-15 14:31:42', '50:8f:4c:44:b2:dd', ''),
(90, '2018-08-16 07:14:14', '00:ee:bd:66:fa:7c', ''),
(91, '2018-08-16 08:01:53', 'f4:60:e2:9c:7a:cc', ''),
(92, '2018-08-16 08:04:22', '00:08:22:8c:04:fc', ''),
(93, '2018-08-16 08:44:04', 'f4:60:e2:9c:7a:cc', ''),
(94, '2018-08-16 09:52:25', 'f4:60:e2:9c:7a:cc', ''),
(95, '2018-08-16 10:00:19', '00:08:22:8c:04:fc', ''),
(96, '2018-08-16 10:06:57', 'f4:60:e2:9c:7a:cc', ''),
(97, '2018-08-16 11:13:34', '00:08:22:8c:04:fc', ''),
(98, '2018-08-16 13:03:14', '00:08:22:8c:04:fc', ''),
(99, '2018-08-16 14:54:47', '00:08:22:8c:04:fc', ''),
(100, '2018-08-16 14:59:25', '00:08:22:8c:04:fc', ''),
(101, '2018-08-16 16:58:01', '00:08:22:8c:04:fc', ''),
(102, '2018-08-17 07:09:26', 'f4:60:e2:9c:7a:cc', ''),
(103, '2018-08-17 07:11:57', '00:ee:bd:66:fa:7c', ''),
(104, '2018-08-17 07:29:35', 'f4:60:e2:9c:7a:cc', ''),
(105, '2018-08-17 08:03:47', 'f4:60:e2:9c:7a:cc', ''),
(106, '2018-08-17 08:23:08', '34:80:b3:3e:63:b7', ''),
(107, '2018-08-17 08:45:27', '00:ee:bd:66:fa:7c', ''),
(108, '2018-08-17 14:14:44', '00:08:22:a4:05:fc', ''),
(109, '2018-08-17 14:31:46', '64:db:43:4e:0c:66', ''),
(110, '2018-08-17 14:37:37', 'b0:e2:35:77:d4:f9', ''),
(111, '2018-08-17 15:13:48', '34:80:b3:3e:63:b7', ''),
(112, '2018-08-18 01:52:41', 'f8:84:f2:55:1e:57', ''),
(113, '2018-08-18 01:52:42', 'f8:84:f2:55:1e:57', ''),
(114, '2018-08-18 05:01:41', '00:08:22:a4:05:fc', ''),
(115, '2018-08-18 09:09:06', '00:08:22:a4:05:fc', ''),
(116, '2018-08-18 09:56:44', '00:08:22:a4:05:fc', ''),
(117, '2018-08-18 14:32:53', '9c:2e:a1:ff:60:27', ''),
(118, '2018-08-18 15:38:22', '00:08:22:a4:05:fc', ''),
(119, '2018-08-18 16:31:03', '00:08:22:a4:05:fc', ''),
(120, '2018-08-18 20:56:27', '9c:2e:a1:ff:60:27', ''),
(121, '2018-08-19 03:01:09', '9c:2e:a1:ff:60:27', ''),
(122, '2018-08-20 08:33:50', 'f4:60:e2:9c:7a:cc', ''),
(123, '2018-08-20 08:33:58', 'f4:60:e2:9c:7a:cc', ''),
(124, '2018-08-20 09:10:56', '00:ee:bd:66:fa:7c', ''),
(125, '2018-08-20 10:29:23', '00:ee:bd:66:fa:7c', ''),
(126, '2018-08-20 10:29:25', '00:ee:bd:66:fa:7c', ''),
(127, '2018-08-20 11:49:46', '50:8f:4c:44:b2:dd', ''),
(128, '2018-08-20 11:49:50', '50:8f:4c:44:b2:dd', ''),
(129, '2018-08-21 07:41:03', '00:ee:bd:66:fa:7c', ''),
(130, '2018-08-21 07:51:18', 'f4:60:e2:9c:7a:cc', ''),
(131, '2018-08-21 08:16:10', '34:80:b3:3e:63:b7', ''),
(132, '2018-08-22 09:22:45', 'f4:60:e2:9c:7a:cc', ''),
(133, '2018-08-22 09:48:13', '2c:fd:ab:13:aa:19', ''),
(134, '2018-08-22 10:44:38', '00:08:22:9e:fc:fb', ''),
(135, '2018-08-22 12:32:50', '34:80:b3:3e:63:b7', ''),
(136, '2018-08-22 12:56:31', '00:08:22:9e:fc:fb', ''),
(137, '2018-08-22 13:21:08', 'b0:e2:35:77:d4:f9', ''),
(138, '2018-08-22 14:26:41', '00:08:22:9e:fc:fb', ''),
(139, '2018-08-23 05:00:34', '50:8f:4c:44:b2:dd', ''),
(140, '2018-08-23 08:00:58', '00:ee:bd:66:fa:7c', ''),
(141, '2018-08-23 08:13:59', '2c:fd:ab:13:aa:19', ''),
(142, '2018-08-23 08:41:26', 'f4:60:e2:9c:7a:cc', ''),
(143, '2018-08-23 11:28:53', '50:8f:4c:44:b2:dd', ''),
(144, '2018-08-25 05:33:35', '34:80:b3:3e:63:b7', ''),
(145, '2018-08-25 12:17:59', '34:80:b3:3e:63:b7', ''),
(146, '2018-08-25 14:31:19', '9c:2e:a1:ff:60:27', ''),
(147, '2018-08-25 20:36:23', '9c:2e:a1:ff:60:27', ''),
(148, '2018-08-26 02:36:33', '9c:2e:a1:ff:60:27', ''),
(149, '2018-08-26 17:15:25', '00:08:22:9e:fc:fb', ''),
(150, '2018-08-27 06:12:47', '34:80:b3:3e:63:b7', ''),
(151, '2018-08-27 13:41:48', '34:80:b3:3e:63:b7', ''),
(152, '2018-08-27 17:34:32', '50:8f:4c:44:b2:dd', ''),
(153, '2018-08-28 11:25:07', '34:80:b3:3e:63:b7', ''),
(154, '2018-08-28 11:52:11', '9c:2e:a1:ff:60:27', ''),
(155, '2018-08-29 05:20:18', '00:08:22:9e:fc:fb', ''),
(156, '2018-08-29 06:53:18', '34:80:b3:3e:63:b7', ''),
(157, '2018-08-29 06:59:15', '00:ee:bd:66:fa:7c', '');

-- --------------------------------------------------------

--
-- Структура таблиці `phones`
--

CREATE TABLE `phones` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `android`
--
ALTER TABLE `android`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `in`
--
ALTER TABLE `in`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `iphone`
--
ALTER TABLE `iphone`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `justin`
--
ALTER TABLE `justin`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `android`
--
ALTER TABLE `android`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблиці `in`
--
ALTER TABLE `in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `iphone`
--
ALTER TABLE `iphone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `justin`
--
ALTER TABLE `justin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT для таблиці `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- База даних: `microtik1`
--
CREATE DATABASE IF NOT EXISTS `microtik1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `microtik1`;

-- --------------------------------------------------------

--
-- Структура таблиці `android`
--

CREATE TABLE `android` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `android`
--

INSERT INTO `android` (`id`, `date`, `mac`) VALUES
(38, '2018-08-09 18:13:08', '18:F0:E4:E7:C9:61'),
(40, '2018-08-12 16:25:53', '88:AD:D2:8B:F0:1F'),
(41, '2018-08-12 18:53:16', '80:AD:16:4D:DB:03'),
(42, '2018-08-16 14:50:19', 'D8:C4:E9:C8:4C:0F'),
(43, '2018-08-22 15:59:59', '30:63:6B:D4:F4:D7'),
(44, '2018-08-23 10:02:19', '7C:2E:DD:3C:7A:92'),
(45, '2018-08-24 13:14:41', '04:F1:28:72:FB:18'),
(46, '2018-08-28 13:21:42', '88:83:22:CA:BE:2E'),
(47, '2018-08-28 14:01:30', '88:83:22:CA:BE:2E'),
(48, '2018-08-28 16:24:19', '20:47:DA:33:23:01');

-- --------------------------------------------------------

--
-- Структура таблиці `in`
--

CREATE TABLE `in` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `iphone`
--

CREATE TABLE `iphone` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `iphone`
--

INSERT INTO `iphone` (`id`, `date`, `mac`) VALUES
(10, '2018-08-11 17:35:13', '50:7A:55:1E:73:4F'),
(11, '2018-08-16 16:52:30', '20:EE:28:F3:4A:48'),
(12, '2018-08-16 16:52:32', '20:EE:28:F3:4A:48'),
(13, '2018-08-22 16:00:11', '30:63:6B:D4:F4:D7'),
(14, '2018-08-23 10:02:30', '8C:29:37:1F:A6:9E'),
(15, '2018-08-23 18:10:06', '58:E2:8F:51:31:2F'),
(16, '2018-08-26 09:25:42', '48:43:7C:6E:74:0A'),
(17, '2018-08-26 09:25:59', '48:43:7C:6E:74:0A'),
(18, '2018-08-26 09:26:17', 'B8:44:D9:6F:48:17');

-- --------------------------------------------------------

--
-- Структура таблиці `justin`
--

CREATE TABLE `justin` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `justin`
--

INSERT INTO `justin` (`id`, `date`, `mac`, `name`) VALUES
(63, '2018-08-11 12:51:39', 'D8:6C:02:9B:7D:A3', ''),
(64, '2018-08-11 16:11:26', 'E4:2B:34:21:5F:69', ''),
(65, '2018-08-12 09:40:32', 'D0:FC:CC:6E:5A:3A', ''),
(66, '2018-08-12 10:17:04', 'D0:FC:CC:6E:5A:3A', ''),
(67, '2018-08-12 13:12:07', '18:F0:E4:FA:DE:3D', ''),
(68, '2018-08-12 17:39:18', '24:F0:94:8E:F7:2D', ''),
(69, '2018-08-13 12:07:43', '3C:E0:72:20:98:92', ''),
(70, '2018-08-13 14:04:05', '3C:E0:72:20:98:92', ''),
(71, '2018-08-13 14:48:23', '3C:E0:72:20:98:92', ''),
(72, '2018-08-16 16:51:20', '4C:B1:99:E4:26:97', ''),
(73, '2018-08-16 16:52:39', '20:EE:28:F3:4A:48', ''),
(74, '2018-08-16 16:52:40', '20:EE:28:F3:4A:48', ''),
(75, '2018-08-20 17:33:11', 'C8:D7:B0:70:F3:2F', ''),
(76, '2018-08-20 18:09:00', '90:21:81:E3:76:79', ''),
(77, '2018-08-20 18:43:24', '48:4B:AA:49:25:21', ''),
(78, '2018-08-20 18:43:38', '48:4B:AA:49:25:21', ''),
(79, '2018-08-22 17:46:28', 'AC:AF:B9:37:2F:EC', ''),
(80, '2018-08-23 16:22:44', '50:04:B8:AB:B4:EE', ''),
(81, '2018-08-26 15:47:04', 'D8:6C:02:A3:17:DB', ''),
(82, '2018-08-26 15:48:47', '84:C7:EA:8C:7E:79', ''),
(83, '2018-08-26 16:52:41', 'DC:D9:16:C0:F7:28', ''),
(84, '2018-08-26 17:34:30', '70:28:8B:25:B6:C2', ''),
(85, '2018-08-26 18:10:26', '70:28:8B:25:B6:C2', ''),
(86, '2018-08-28 13:45:25', '70:11:24:C8:6B:F2', '');

-- --------------------------------------------------------

--
-- Структура таблиці `phones`
--

CREATE TABLE `phones` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `phones`
--

INSERT INTO `phones` (`id`, `date`, `mac`) VALUES
(1, '2018-08-17 10:22:43', '44:74:6C:41:D2:DE'),
(2, '2018-08-20 15:42:14', '40:40:A7:4A:FA:51'),
(3, '2018-08-20 15:54:18', '40:40:A7:4A:FA:51');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `android`
--
ALTER TABLE `android`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `in`
--
ALTER TABLE `in`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `iphone`
--
ALTER TABLE `iphone`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `justin`
--
ALTER TABLE `justin`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `android`
--
ALTER TABLE `android`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT для таблиці `in`
--
ALTER TABLE `in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `iphone`
--
ALTER TABLE `iphone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблиці `justin`
--
ALTER TABLE `justin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT для таблиці `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- База даних: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Структура таблиці `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

--
-- Дамп даних таблиці `pma__navigationhiding`
--

INSERT INTO `pma__navigationhiding` (`username`, `item_name`, `item_type`, `db_name`, `table_name`) VALUES
('root', 'in', 'table', 'base', '');

-- --------------------------------------------------------

--
-- Структура таблиці `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Дамп даних таблиці `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2018-08-29 10:15:37', '{\"lang\":\"uk\",\"Console\\/Mode\":\"collapse\"}');

-- --------------------------------------------------------

--
-- Структура таблиці `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Структура таблиці `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Індекси таблиці `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Індекси таблиці `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Індекси таблиці `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Індекси таблиці `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Індекси таблиці `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Індекси таблиці `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Індекси таблиці `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Індекси таблиці `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Індекси таблиці `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Індекси таблиці `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Індекси таблиці `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Індекси таблиці `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Індекси таблиці `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Індекси таблиці `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Індекси таблиці `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Індекси таблиці `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Індекси таблиці `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- База даних: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
